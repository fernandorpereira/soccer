/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

/**
 *
 * @author Rezaei
 */
public class Utility {

    public static int sgn(Double n) {
        if (n > 0) {
            return 1;
        }
        if (n < 0) {
            return -1;
        }
        return 0;
    }
public static int sgn(int n) {
        if (n > 0) {
            return 1;
        }
        if (n < 0) {
            return -1;
        }
        return 0;
    }
    public static int min(double[] elements) {
        double min = elements[0];
        int j, i = 0;
        for (j = 1; j < elements.length; j++) {
            if (elements[j] < min) {
                min = elements[j];
                i = j;
            }
        }
        return i;
    }
}
