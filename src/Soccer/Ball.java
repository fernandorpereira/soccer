/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;
import javax.swing.Timer;
import Kinematic.Vector;

/**
 *
 * @author Rezaei
 */
public class Ball extends JRadioButton {
    /**
     * Zarib estekak ba chaman
     */
    private  Double US= 0.3;
    private Timer animationTimer; // Timer drives animation
    private int RefreshRate = 50;
    private String Status = "";
    private Point To;
    protected Vector Loc = new Vector(0, 0), From = new Vector(0, 0);
    private Pitch OnPitch;
    private double Teta;
    /**
     * Current Velocity of ball(m/s)
     */
    private Vector Velocity;
    /**
     * Current Acceleration of ball(m/s2)
     */
    private Vector Acceleration;
    /**
     * Mass of ball in Kilo gram's
     */
    private double Mass = 1;
    /**
     * Current Total Force on ball(N)
     */
    private Vector Force;

    public Ball(Pitch P) {
        OnPitch = P;
        this.Velocity = new Vector(0, 0);
        this.Acceleration = new Vector(0, 0);
        this.Force=new Vector(0, 0);
        this.setLoc(OnPitch.getCenter());
        this.animationTimer = new Timer(RefreshRate, new Ball.TimerHandler());
        this.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Soccer-Ball.png"))); // NOI18N
        this.setSize(10, 10);
    }

    public Ball() {
        this.US = 0.4;
        OnPitch = new Pitch();
        this.Velocity = new Vector(0, 0);
        this.Acceleration = new Vector(0, 0);
        this.setLoc(OnPitch.getCenter());
        this.animationTimer = new Timer(RefreshRate, new Ball.TimerHandler());
        this.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Soccer-Ball.png"))); // NOI18N
        this.setSize(10, 10);
    }

    public void Play() {

        animationTimer.start();
    }

    protected void CalculateTotalForce() {
        if (Velocity.Size() > 0.01) {
            Double Fs=Mass*Kinematic.Constants.GroundAcc*US;
            /*Force.x = -1*Fs*Math.cos(Teta);
            Force.y =  -1*Fs*Math.sin(Teta);
            
             * Force.x = -1*Utility.sgn(Velocity.x) *Fs*Math.cos(Teta);
            Force.y =  -1*Utility.sgn(Velocity.y) *Fs*Math.sin(Teta);
            */
            Force.x = -Fs*Velocity.x/Velocity.Size();
            Force.y =  -Fs*Velocity.y/Velocity.Size();
        }else{
            Force.x = 0.0;
            Force.y = 0.0;
        }
    }

    protected void CalculateAcceleration() {
        Acceleration.x = Force.x / Mass;
        Acceleration.y = Force.y / Mass;
    }

    public void TakeImpact(Point To, double F) {
        Force = new Vector(0, 0);
        From = this.getLoc();
        Teta = (double) (To.y - From.y) / (To.x - From.x);
        Force.x += F * Math.cos(Math.atan(Teta));
        Force.y += F * Math.sin(Math.atan(Teta));
        CalculateAcceleration();
        Move(To);
    }

    public void Move(java.awt.Point p) {
        //Status = "Move";
        To = p;
        From = this.getLoc();
        Teta = (double) (To.y - From.y) / (To.x - From.x);
    }

    public void nextFrame() {
        //if (Status.startsWith("Move")) {
            Vector Loc = this.getLoc();
            double t = RefreshRate / 1000.0;
            double nimt2 = 0.5 * Math.pow(t, 2);
            double newX = Acceleration.x * nimt2 + Velocity.x * t + Loc.x;
            double newY = Acceleration.y * nimt2 + Velocity.y * t + Loc.y;
            if (newX >= -OnPitch.getWidth() / 2 && newX <= OnPitch.getWidth() / 2 && newY >= -OnPitch.getHeight() / 2 && newY <= OnPitch.getHeight() / 2) {
                this.setLoc(newX, newY);
                Velocity.x = Acceleration.x * t + Velocity.x;
                Velocity.y = Acceleration.y * t + Velocity.y;
                CalculateTotalForce();
                CalculateAcceleration();
            }
/*
        } else {
            String s;
            s = Status;
        }*/
    }

    public void nextFrameOld() {
        switch (Status) {
            case "Move":
                Point p = this.getLocation();
                int i = 0,
                 j = 0,
                 m = 0;
                if (p != To) {
                    i = To.x - p.x;
                    j = To.y - p.y;
                    //m=j/i;
                }
                this.setLocation(p.x + (Utility.sgn(i)), p.y + (Utility.sgn(j)));
        }
        /*
         images[ currentImage ].paintIcon( this, this.getGraphics(), 0, 0 );

         // set next image to be drawn only if timer is running
         if ( animationTimer.isRunning() )
         currentImage = ( currentImage + 1 ) % TOTAL_IMAGES;*/
    }

    // start animation, or restart if window is redisplayed
    public void startAnimation() {
        if (animationTimer.isRunning()) {
            animationTimer.restart();
        } // end if
        else // animationTimer already exists, restart animation
        {
            //currentImage = 0; // display first image
            animationTimer.start(); // start timer
        } // end else
    } // end method startAnimation

    // stop animation timer
    public void stopAnimation() {
        animationTimer.stop();
    } // end method stopAnimation

    public Vector getLoc() {
        return Loc;
    }

    public void setLoc(Vector Loc) {
        this.Loc = Loc;
        this.setLocation(Loc.Translate(OnPitch.getCenter()));
    }

    public void setLoc(double x, double y) {
        this.Loc.x = x;
        this.Loc.y = y;
        Point p = Loc.Translate(OnPitch.getCenter());
        this.setLocation(p.x - this.getWidth() / 2, p.y - this.getHeight() / 2);
    }

    public void setLoc(Point Loc) {
        this.setLocation(Loc.x - this.getWidth() / 2, Loc.y - this.getHeight() / 2);
        Point Center = OnPitch.getCenter();
        Loc.translate(-Center.x, -Center.y);
        this.Loc.x =Double.valueOf(Loc.x);
        this.Loc.y = Double.valueOf(Loc.y);
    }
    
    public String log(){
    return "a.x="+Acceleration.x.toString()+" v="+Velocity.Size().toString();
    }

    // inner class to handle action events from Timer
    private class TimerHandler implements ActionListener {
        // respond to Timer's event

        public void actionPerformed(ActionEvent actionEvent) {
            //TakeImpact();
            nextFrame(); // repaint animator
            // AirPortApp.getApplication().View.getConsole().Log("animation frame");
        } // end method actionPerformed
    }
}
