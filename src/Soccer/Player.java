/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;
import javax.swing.Timer;

/**
 *
 * @author Rezaei
 */
public class Player extends JRadioButton {

    private Timer animationTimer; // Timer drives animation
    protected String Status = "";
    private Point To, From;
    private double Teta;
    protected Pitch OnPitch;
    public Team InTeam;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
        this.setText(Status);
    }

    public Player() {
        this.animationTimer = new Timer(50, new TimerHandler());
        //this.InTeam=team;
    }

    public void Play(Pitch P) {
        OnPitch = P;
        animationTimer.start();
    }

    private void MakeDecision() {
        if (getLocation().distance(OnPitch.ball.getLocation().x, OnPitch.ball.getLocation().y) == 0) {
            Point Goal = OnPitch.getGoal(this.InTeam.Side.getOppositeSide());
            if (OnPitch.ball.getLoc().Distance(Goal.x, Goal.y) < (OnPitch.getWidth() / 2)) {
                Shoot(Goal);
            } else {
                PaBeToop(Goal);
            }

        } else /*if (!Status.startsWith("Moving"))*/ {
            Move(OnPitch.ball.getLocation());
        }
    }

    public void Move(java.awt.Point p) {
        setStatus("Moving to " + p.x + "," + p.y);
        To = p;
        From = this.getLocation();
        Teta = (double) (From.y - To.y) / (To.x - From.x);

    }

    public void PaBeToop(java.awt.Point p) {
        setStatus("Pa be toop " + p.x + "," + p.y);
        //To = p;
        //From = this.getLocation();
        //Teta = (double) (From.y - To.y) / (To.x - From.x);
        OnPitch.ball.TakeImpact(p, 90);
    }

    protected void Shoot(Point to) {
        setStatus("Shooting");
        OnPitch.ball.TakeImpact(to, 900);
    }

    public void nextFrame() {
        if (Status.startsWith("Moving")) {
            Point Now = this.getLocation();
            if (Now.distance(To.x, To.y) != 0) {
                if (To.x - From.x != 0) {
                    int newX = Now.x + (Utility.sgn(To.x - Now.x));
                    int NewY = (int) Math.round(-Teta * (newX - To.x) + To.y);
                    //int NewY= p.y + (Utility.sgn(j));
                    this.setLocation(newX, NewY);
                } else {
                    this.setLocation(To.x, Now.y + (Utility.sgn(To.y - From.y)));
                }
            }

        } else {
            String s;
            s = Status;
        }
        /*
         images[ currentImage ].paintIcon( this, this.getGraphics(), 0, 0 );

         // set next image to be drawn only if timer is running
         if ( animationTimer.isRunning() )
         currentImage = ( currentImage + 1 ) % TOTAL_IMAGES;*/
    }

    // start animation, or restart if window is redisplayed
    public void startAnimation() {
        if (animationTimer.isRunning()) {
            animationTimer.restart();
        } // end if
        else // animationTimer already exists, restart animation
        {
            //currentImage = 0; // display first image
            animationTimer.start(); // start timer
        } // end else
    } // end method startAnimation

    // stop animation timer
    public void stopAnimation() {
        animationTimer.stop();
    } // end method stopAnimation

    // inner class to handle action events from Timer
    private class TimerHandler implements ActionListener {
        // respond to Timer's event

        public void actionPerformed(ActionEvent actionEvent) {
            MakeDecision();
            nextFrame(); // repaint animator
            // AirPortApp.getApplication().View.getConsole().Log("animation frame");
        } // end method actionPerformed
    }
}
