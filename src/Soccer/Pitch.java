/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Soccer;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Rezaei
 */
public class Pitch extends ImagePanel {
    //private Point Goal;
public Ball ball;


    public Point getCenter() {
        return new Point(this.getWidth()/2, this.getHeight()/2);
    }

    public Pitch() {
        this.setBackground(Color.green);
        this.setSize(480, 320);
        this.ball=new Ball(this);
        ball.setLoc(0, 0);
        this.add(ball);
        //this.imageUpdate(new Image, WIDTH, WIDTH, WIDTH, WIDTH, WIDTH);
    }

    public Point getGoal(DefendingSide Side) {
        switch(Side){
            case Left:
                return new Point(-this.getWidth()/2+20, 0);
            case Right:
                return new Point(this.getWidth()/2-20, 0);
                default:
                    return new Point();
        }
        
        
    }


}
