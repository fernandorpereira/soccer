/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Kinematic;

import java.awt.Point;

/**
 *
 * @author Rezaei
 */
public class Vector {

    public Double x;
    public Double y;

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector Translate(double dx, double dy) {
        return new Vector(this.x - dx, this.y - dy);
    }

    public Point Translate(Point dp) {
        return new Point((int) (this.x + dp.x), (int) (dp.y - this.y));
    }

    public Double Size() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public Double Distance(Double x, Double y) {
        return Math.sqrt(Math.pow((this.x - x), 2) + Math.pow((this.y - y), 2));
    }
public Double Distance(int x, int y) {
        return Math.sqrt(Math.pow((this.x - x), 2) + Math.pow((this.y - y), 2));
    }
}
